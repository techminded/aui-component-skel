YUI.add('aui-component-skel', function(Y) {
    // psuedo constants
    // component parameters
    var NS = "ns",
        PARAMS = "params",
        ELS = "els",
        L11N = "l11n",

        LANG = Y.Lang, // convenience assignment

        TPL_AUI_COMPONENT_SKEL = '<div class="{sl}">{label}</div>' // template
        ;

    Y.AuiComponentSkel = Y.Component.create({
        NAME: 'auiComponentSkel',
        EXTENDS: Y.Component,
        ATTRS: {
            ns: {},
            params: {},
            els: {},
            l11n: {}
        },
        prototype: {

            getNs: function() {
                return this.get(NS);
            },

            getParams: function() {
                return this.get(PARAMS);
            },

            getEl: function(key) {
                return this.get(ELS)[key];
            },
            /**
             * To access translation string passed as parameter
             * @param key
             * @returns {*}
             */
            tr: function(key) {
                var instance = this
                ;

                translated = instance.get(L11N)[key];
                if (translated == undefined) {
                    console.warn('aui skel component translation not found ', key);
                }

                return translated ? translated : key;
            },

            /**
             * To bind event handlers, will be called automagically during component rendering
             */
            bindUI: function() {
                var instance = this
                ;
                // live reverse binding with component as context
                Y.one('body').on('click', Y.rbind(instance.onAuiComponentClicked, instance), instance.getEl('auiComponentSl'));
            },

            /**
             * To place rendering logic, will be called automagically during component rendering
             */
            renderUI: function () {
                var instance = this
                ;
                // template rendering
                Y.one('body').append(LANG.sub(TPL_AUI_COMPONENT_SKEL, {

                    sl: instance.getEl('auiComponentSl'),
                    label: instance.tr('helloLabel')

                }));

                console.debug('aui skel componend loaded');
                console.debug('jquery', $);
            },
            /**
             * Event handler
             * @param event - merged event attributes object
             */
            onAuiComponentClicked: function(event) {
                alert('clicked');
            }
        }
    });

}, '1.0', { requires: ['aui-base', 'aui-component', 'node', 'event', 'jquery']}); // mandatorary requirements